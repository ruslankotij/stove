const gulp   = require('gulp'),
browserSync  = require('browser-sync').create(),
pug          = require('gulp-pug'),
sass         = require('gulp-sass'),
spritesmith  = require('gulp.spritesmith'),
autoprefixer = require('gulp-autoprefixer'),
imagemin     = require('gulp-imagemin'),
del          = require('del'),
concat       = require('gulp-concat'),
sourcemaps   = require('gulp-sourcemaps'),
uglify       = require('gulp-uglify-es').default,
cssMedia     = require('gulp-group-css-media-queries'),
smartgrid    = require('smart-grid');
//----------------Delete-------------------
gulp.task('clean', async function() {
    return del.sync('build');
});

//--------------Server--------------------------
gulp.task('server', function() {
    browserSync.init({
        server: {
            port: 9000,
            baseDir: "app"
        }
    });
    gulp.watch("app/sass/**/*.sass", {usePolling: true}, gulp.series('sass'));
    gulp.watch('app/template/**/*.pug', {usePolling: true}, gulp.series('template'));
    gulp.watch(["app/js/*.js", '!app/js/main.min.js'], {usePolling: true}, gulp.series('script'));
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

//--------Pug complite---------------
gulp.task('template', function buildHTML() {
    return gulp.src('app/template/index.pug')
        .pipe(pug({
            pretty:true
        }))
        .pipe(gulp.dest('app'))
        .pipe(browserSync.stream());
});

//----------------Sass----------------------

gulp.task('sass', function() {
    return gulp.src("app/sass/*.sass")
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer(['last 5 versions', 'ie 8'], {cascade: true}))
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});
//------------Script---------------------

gulp.task('script', function() {
    return gulp.src([
            'app/js/init.js',
            'app/js/navigation.js',
            'app/js/validation.js',
            'app/js/slider.js',
            'app/js/main.js'
            
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/js'))
        .pipe(browserSync.reload({stream: true}));


});
//----------Sprites-----------------------


gulp.task('sprite', function () {
    var spriteData = gulp.src('app/img/icon/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../img/sprite.png',
        cssName: 'sprite.sass'
    })
    );

     return spriteData.img.pipe(gulp.dest('app/img/')),
            spriteData.css.pipe(gulp.dest('app/sass/global/'));

});

//-------------Img mimifider-------------------

gulp.task('img', () =>
    gulp.src('app/img/**/*.*')
        .pipe(imagemin([
    imagemin.gifsicle({interlaced: true}),
    imagemin.jpegtran({progressive: true}),
    imagemin.optipng({optimizationLevel: 5}),
    imagemin.svgo({
        plugins: [
            {removeViewBox: true},
            {cleanupIDs: false}
        ]
    })
]))
        .pipe(gulp.dest('build/img'))
);


//------------Css Libs-------------------------

// gulp.task('css-libs', ['sass'], function() {
// 		return gulp.src('app/css/libs.css')
// 				.pipe(cssnano())
// 				.pipe(rename({suffix: '.min'}))
// 				.pipe(gulp.dest('app/css'));
// });
//--------------Copy-------------------------

gulp.task('copy', async function() {


        gulp.src('app/css/main.css')
            .pipe(cssMedia())
            .pipe(gulp.dest('build/css'));
          
        gulp.src('app/fonts/**/*')
            .pipe(gulp.dest('build/fonts'));

        gulp.src('app/libs/**/*')
            .pipe(gulp.dest('build/libs'));

        gulp.src('app/js/*.js')
            .pipe(gulp.dest('build/js'));

        gulp.src('app/*.html')
            .pipe(gulp.dest('build'));
});

//--------------Smart-grid----------------------
var settings = {
    outputStyle: 'sass', /* less || scss || sass || styl */
    columns: 12, /* number of grid columns */
    offset: '30px', /* gutter width px || % || rem */
    mobileFirst: false, /* mobileFirst ? 'min-width' : 'max-width' */
    container: {
        maxWidth: '1200px', /* max-width оn very large screen */
        fields: '30px' /* side fields */
    },
    breakPoints: {
        lg: {
            width: '1100px', /* -> @media (max-width: 1100px) */
        },
        md: {
            width: '960px'
        },
        sm: {
            width: '780px',
            fields: '15px' /* set fields only if you want to change container.fields */
        },
        xs: {
            width: '560px'
        }
        /*
        We can create any quantity of break points.

        some_name: {
            width: 'Npx',
            fields: 'N(px|%|rem)',
            offset: 'N(px|%|rem)'
        }
        */
    }
};

smartgrid('./libs/smart-grid', settings);

//--------------Build-------------------------
gulp.task('build', gulp.series(
    gulp.parallel('clean', 'img', 'sass', 'script'),
    gulp.parallel('copy')
    )
    );

//--------------Derault---------------------
gulp.task('default', gulp.series(
    gulp.parallel('template', 'sass', 'sprite', 'script'),
    gulp.parallel('server')
));


